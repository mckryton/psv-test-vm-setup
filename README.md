# Virtual machine setup as test environment for [PSV](https://codeberg.org/japh/psv)

This repository provides the setup for a virtual machine that can be used as test environment for the [PSV](https://codeberg.org/japh/psv) utility. Therefore the vm will be provisioned with the most recent version of [GO](https://go.dev/) as well as with the latest release of [PSV](https://codeberg.org/japh/psv).

## Prerequisites
To create the [PSV](https://codeberg.org/japh/psv) virtual test system you have to install the following tools on you local system:

* [Vagrant](https://www.vagrantup.com/) 2.2.6
* [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/index.html) 2.9.6
* [Virtualbox](https://www.virtualbox.org/) 6.1.26

The setup was developed using those version. Newer versions of the tools above might require adaption in the config files.

## Installation
When your local machine does meet all the prerequisites the next step is to clone this repository. You may then open a terminal at the location of this README and enter the command `vagrant up`. That's it. Vagrant will now create a virtual machine and use Ansible to install [GO](https://go.dev/) and [PSV](https://codeberg.org/japh/psv) on it.

## Usage

### Functional Testing
Log into the machine with `vagrant ssh` and enter `cd source/psv` to make the [PSV](https://codeberg.org/japh/psv) repository your current directory.
Enter `go test` to run all available tests.

### Build the command line application
n/a
